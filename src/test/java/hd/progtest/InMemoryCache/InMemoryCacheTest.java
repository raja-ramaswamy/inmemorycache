package hd.progtest.InMemoryCache;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.junit.Test;

/**
 * Unit tests for InMemoryCache
 */
public class InMemoryCacheTest {


	@Test
	public void addJsonToCache_checkSizeOfCache() {
		String data = readJsonDataFromFile("json-sample.json");		
		InMemoryCache.add("json-sample.json", data);		
		assertEquals(InMemoryCache.size(), 1);		
	}
	
	@Test
	public void getJsonDataFromCache_checkWithJsonDataInDisk() {
		String dataFromCache = (String) InMemoryCache.get("json-sample.json");
		String dataFromDisk = readJsonDataFromFile("json-sample.json");		
		assertEquals(dataFromCache.length(), dataFromDisk.length());
	}
	
	@Test
	public void addXmlDataToCache_checkSizeOfCache_sizeMustBe2() {
		String data = readJsonDataFromFile("xml-sample.xml");		
		InMemoryCache.add("xml-sample.xml", data);		
		assertEquals(InMemoryCache.size(), 2);			
	}
	
	private String readJsonDataFromFile(String filename) {
		StringBuffer jsonContents = new StringBuffer();

		String path = System.getProperty("user.dir");
		path += "/data/" + filename;

		try (BufferedReader br = new BufferedReader(new FileReader(path))) {
			while (br.readLine() != null) {
				jsonContents.append(br.readLine());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String data = jsonContents.toString();
		return data;
	}
}
