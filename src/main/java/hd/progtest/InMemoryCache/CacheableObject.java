package hd.progtest.InMemoryCache;

import java.util.Date;

/*
 * CacheableObject will have data to be cached and expiry date
 * This class can be extended to hold additional properties for each cacheable element 
 * 
 * CacheableObject is built using Builder pattern. While it may increase code complexity to have 
 * builder pattern to build a class with only 2 properteis, it will help when the class is extended to 
 * have more properties later. 
 */

public class CacheableObject {

	private Object data; 
	private Date expiry;

	
	private CacheableObject(Object data, Date expiry) {
		this.data = data;
		this.expiry = expiry;
	}
	
	public Object getData() {
		return data;
	}

	public Date getExpiry() {
		return expiry;
	}

	public static class Builder {
		
		private Object data;
		private Date expiry;
		
		public Builder data(Object data) {
			this.data = data;
			return this;
		}
		
		public Builder expiry(int ttlInSecs) {
			long ttlInMS= ttlInSecs * 1000;
			this.expiry = new Date(ttlInMS);
			return this;
		}
		
		public CacheableObject build() {
			return new CacheableObject(data,expiry);
		}
		
	}
	
}
