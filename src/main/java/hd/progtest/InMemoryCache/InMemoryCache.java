package hd.progtest.InMemoryCache;

import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

/**
 * In-Memory Cache - similar to HTTP header cache with max-age (timeToLive)
 * 
 * Cache : A concurrent hashmap to hold data from any file format (xml,json, docx, img etc) 
 *         key,value = filename, data(xml,json,img..)
 *         
 * Expiry : Data in cache have an expiry equal to current time plus time to live 
 * 
 * Purge : A separate thread will scan the cache elements and will remove elements that are exprired 
 * 
 * Configurable properties : timeToLive and Purge interval can be modified in cache-properties.txt 
 * 
 * Clear cache : Cache can be cleared from CacheUtil
 *
 */
public class InMemoryCache {

	private static ConcurrentHashMap<String, Object> cache = new ConcurrentHashMap<>();
	private static int timeToLiveInSecs = 60; // default value
	private static int purgeIntervalInSecs = 300; // default value

	static {

		Properties cacheProperties = CacheUtil.getCacheProperties();
		timeToLiveInSecs = Integer.parseInt(cacheProperties.getProperty("timeToLiveInSecs"));
		purgeIntervalInSecs = Integer.parseInt(cacheProperties.getProperty("purgeIntervalInSecs"));

		purge();

	}

	public static void add(String filename, Object data) {

		if (filename == null) {
			throw new NullPointerException("Filename cannot be null");
		}

		if (data == null) {
			throw new NullPointerException("Data cannot be null");
		}

		CacheableObject co = new CacheableObject.Builder().data(data).expiry(timeToLiveInSecs).build();
		cache.put(filename, co);		
	}

	public static Object get(String filename) {

		Object data = null;
		CacheableObject co = (CacheableObject) cache.get(filename);
		if (co != null)
			data = co.getData();
		return data;

	}

	private static void purge() {

		Thread t = new Thread(new Runnable() {

			volatile boolean flag = true;

			@Override
			public void run() {
				while (flag) {
					if (!cache.isEmpty()) {
						Iterator itr = cache.entrySet().iterator();
						while (itr.hasNext()) {
							Map.Entry pair = (Map.Entry) itr.next();
							CacheableObject co = (CacheableObject) pair.getValue();
							if (co.getExpiry().before(new Date(System.currentTimeMillis()))) {
								itr.remove();
							}
						}
					}
					try {
						Thread.sleep(purgeIntervalInSecs * 1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}

			}

		});
		t.setDaemon(true);
		t.start();

	}

	public static int size() {
		return cache.size();
	}

	static void clear() {
		cache.clear();
	}

	static void resetIntervals(int newTimeToLiveInSecs, int newPurgeIntervalInSecs) {
		timeToLiveInSecs = newTimeToLiveInSecs;
		purgeIntervalInSecs = newPurgeIntervalInSecs;
	}

}
