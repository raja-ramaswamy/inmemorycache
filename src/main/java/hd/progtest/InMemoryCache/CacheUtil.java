package hd.progtest.InMemoryCache;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/*
 * This class provides 2 functionalities 
 * 
 * 1. Reset/configure timeToLive and purgeInterval based on values in cache-properties.txt 
 *    without the need to restart jvm
 * 2. Clear / remove all cache contents without the need to restart jvm 
 */

public class CacheUtil {

	private Properties props;

	public CacheUtil() {

		props = getCacheProperties();

	}

	public static Properties getCacheProperties() {

		String path = System.getProperty("user.dir");
		path += "/props/cache-properties.txt";

		Properties props = new Properties();
		try (BufferedReader br = new BufferedReader(new FileReader(path))) {

			props.load(br);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return props;
	}

	public void resetIntervals() {

		int timeToLiveInSecs = Integer.parseInt(props.getProperty("timeToLiveInSecs"));
		int purgeIntervalInSecs = Integer.parseInt(props.getProperty("purgeIntervalInSecs"));

		InMemoryCache.resetIntervals(timeToLiveInSecs, purgeIntervalInSecs);

	}

	public void clearCache() {
		if (InMemoryCache.size() > 0) {
			InMemoryCache.clear();
		}
	}

	
	public static void main(String[] args) { 
		
		if (args.length > 0) {
			
			CacheUtil cacheUtil = new CacheUtil();
			
			for (String arg : args) {

				switch (arg) {

				case "resetIntervals":
					cacheUtil.resetIntervals();
					break;

				case "clearCache":
					cacheUtil.clearCache();
					break;
				}

			}
			
		}		
	}
}
